//
//  ApiServices.swift
//  testwork
//
//  Created by muxammed on 11/11/2019.
//  Copyright © 2019 muxammed. All rights reserved.
//

import UIKit


class ApiServices:NSObject{
    
    static let sharedInstance = ApiServices()
    
    func searchData(searchString:String,completion:@escaping (Result<SearchResult>)->()){
        
        let url = "https://api.scryfall.com/cards/search?q=o:\(searchString)".encodedUrl()
        var request = URLRequest(url: url!)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {
                    completion(.Error("Hata olustu"))
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {
                completion(.Error("Yene hata kodlu \(response.statusCode)"))
                
                return
            }
            
            let allData = try! JSONDecoder.init().decode(SearchResult.self, from: data)
            
            DispatchQueue.main.async {
                completion(.Success(allData))
                
            }
        }
        task.resume()
        
    }
    
}
