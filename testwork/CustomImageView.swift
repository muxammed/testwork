//
//  CustomImageView.swift
//  testwork
//
//  Created by muxammed on 11/11/2019.
//  Copyright © 2019 muxammed. All rights reserved.
//

import UIKit



class CustomImageView: UIImageView {
    
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    var imageUrlString: String?
    let indicator:UIActivityIndicatorView = {
        let ind = UIActivityIndicatorView()
        ind.hidesWhenStopped = true
        ind.style = UIActivityIndicatorView.Style.gray
        ind.translatesAutoresizingMaskIntoConstraints = false
        return ind
    }()
    
    
    func loadImageUsingUrlString(urlString: String){
        
        imageUrlString = urlString
        self.addSubview(indicator)
        self.addConstraint(NSLayoutConstraint.init(item: indicator, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint.init(item: indicator, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        indicator.startAnimating()
        
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            indicator.stopAnimating()
            return
        }
        
        
        
        let url = urlString.encodedUrl()
        URLSession.shared.dataTask(with: url!,completionHandler: {
            (data,response,error) in
            if error != nil {
                print(error ?? "error")
                DispatchQueue.main.async {
                    self.indicator.stopAnimating()
                }
                
                return
            }
            
            
            
            DispatchQueue.main.async {
                //do something that must be done on the main queue
                
                self.indicator.stopAnimating()
                
                let imageToCache = UIImage(data: data!)
                
                if self.imageUrlString == urlString {
                    self.image = imageToCache
                }
                if (imageToCache != nil){
                    self.imageCache.setObject(imageToCache!, forKey: urlString as AnyObject)
                }
                
            }
            
            
        })
            .resume()
        
        
        
        
        
        
        
    }
}
