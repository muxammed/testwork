//
//  ViewController.swift
//  testwork
//
//  Created by muxammed on 11/11/2019.
//  Copyright © 2019 muxammed. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    let obodok:UIView = {
        let v = UIView()
        v.layer.borderWidth = 1
        v.layer.borderColor = UIColor.lightGray.cgColor
        v.layer.cornerRadius = 7
        return v
    }()
    let searchField:UITextField = {
        let sf = UITextField()
        sf.placeholder = "text to search"
        sf.layer.borderColor = UIColor.gray.cgColor
        return sf
    }()
    
    let cardsCollection:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 80, right: 10)
        let cv = UICollectionView(frame: .zero,collectionViewLayout: layout)
        cv.backgroundColor = .white
        return cv
    }()
    
    let indicator:UIActivityIndicatorView = {
        let ind = UIActivityIndicatorView()
        ind.style = UIActivityIndicatorView.Style.gray
        ind.hidesWhenStopped = true
        return ind
    }()
    
    let searchButton:UIButton = {
        let btn = UIButton()
        btn.setTitle("Search", for: .normal)
        btn.layer.cornerRadius = 20
        btn.backgroundColor = .blue
        btn.setTitleColor(.white, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    var bottomAnchor:NSLayoutConstraint?
    var cards:[Card]?
    let cellid = "cellid"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupViews()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupViews(){
        self.title = "Test Work"
        view.backgroundColor = .white
        
        
        view.addSubview(obodok)
        view.addSubview(cardsCollection)
        view.addSubview(indicator)
        view.addSubview(searchButton)
        obodok.addSubview(searchField)
        indicator.center = CGPoint(x: view.frame.width/2, y: view.frame.height/2)
        
        
        view.addConsWithFormat(format: "H:|-10-[v0]-10-|", views: obodok)
        view.addConsWithFormat(format: "V:|-10-[v0][v1]|", views: obodok,cardsCollection)
        view.addConsWithFormat(format: "V:[v0(40)]", views: searchButton)
        view.addConsWithFormat(format: "H:|[v0]|", views: cardsCollection)
        view.addConsWithFormat(format: "H:|-50-[v0]-50-|", views: searchButton)
        
        bottomAnchor = NSLayoutConstraint(item: searchButton, attribute: .bottom, relatedBy: .greaterThanOrEqual, toItem: view, attribute: .bottom, multiplier: 1, constant: -10)
        view.addConstraint(bottomAnchor!)
        
        
        obodok.addConsWithFormat(format: "H:|-5-[v0]-5-|", views: searchField)
        obodok.addConsWithFormat(format: "V:|-5-[v0]-5-|", views: searchField)
        
        searchButton.addTarget(self, action: #selector(searchButtonAction), for: .touchUpInside)
        
        cardsCollection.dataSource = self
        cardsCollection.delegate = self
        cardsCollection.register(CardCell.self, forCellWithReuseIdentifier: cellid)
    }

    
    @objc func searchButtonAction(){
        view.endEditing(true)
        
        if let textToSearch = self.searchField.text {
            if (textToSearch == ""){
                
                let alert = UIAlertController(title: "Warning", message: "Please enter a text for search", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
                
                
            } else {
                self.indicator.startAnimating()
                self.loadSearchedData(searchString: textToSearch)
                
            }
        }
    }
    
    func loadSearchedData(searchString:String){
        ApiServices.sharedInstance.searchData(searchString: searchString, completion: { (result) in
            switch result {
            case .Error(let err):
                DispatchQueue.main.async {
                    self.indicator.stopAnimating()
                    let alert = UIAlertController(title: "Error from API", message: "Error recieved from API. \(err)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true)
                }
                
            case .Success(let data):
                DispatchQueue.main.async {
                    self.indicator.stopAnimating()
                    self.cards = data.data
                    self.cardsCollection.reloadData()
                    print("cards count \(data.totalCards)")
                }
                
            }
        })
        
    }
    
    @objc func handleKeyboardNotification(notification:NSNotification){
        
        if let userInfo = notification.userInfo {
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            self.bottomAnchor?.constant = isKeyboardShowing ? -(keyboardFrame.height + 20) : -20
            
            if let scrollView:UIScrollView = self.view as? UIScrollView {
                scrollView.contentSize = isKeyboardShowing ? CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height + keyboardFrame.height) : CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height)
                
                //scrollView.contentOffset = isKeyboardShowing ? CGPoint(x:0, y:self.uphone.frame.origin.y + 30) : CGPoint(x:0, y:self.view.frame.origin.y)
            }
           
            
            UIView.animate(withDuration: 0 , delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }) { (comleted) in
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cards?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! CardCell
        
        cell.oracle_name.text = self.cards![indexPath.item].oracleText ?? "no oracle_text"
        cell.cardPicture.loadImageUsingUrlString(urlString: self.cards![indexPath.item].imageUris?.normal ?? "")
        cell.card_name.text = self.cards![indexPath.item].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let oracle_text = self.cards?[indexPath.item].oracleText ?? ""
        let card_name = self.cards?[indexPath.item].name ?? ""
        let hh = card_name.height(withConstrainedWidth: collectionView.frame.width - 20, font: UIFont.systemFont(ofSize: 15, weight: .bold))
        let hh2 = oracle_text.height(withConstrainedWidth: collectionView.frame.width - 20, font: UIFont.systemFont(ofSize: 13, weight: .regular))
        
        return CGSize(width: collectionView.frame.width - 36, height: hh + hh2 + 232 + 20)
        
    }

}


class CardCell: UICollectionViewCell {
    
    let oracle_name:UILabel = {
        let lb = UILabel()
        lb.numberOfLines = 0
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.lineBreakMode = NSLineBreakMode.byWordWrapping
        lb.minimumScaleFactor = 0.5
        lb.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        return lb
    }()
    
    let cardPicture:CustomImageView = {
        let im = CustomImageView()
        im.contentMode = UIImageView.ContentMode.scaleAspectFit
        im.clipsToBounds = true
        im.layer.cornerRadius = 7
        return im
    }()
    
    let card_name:UILabel = {
        let lb = UILabel()
        lb.numberOfLines = 0
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.lineBreakMode = NSLineBreakMode.byWordWrapping
        lb.minimumScaleFactor = 0.5
        lb.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        lb.textAlignment = .center
        return lb
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        
        layer.cornerRadius = 7
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 1
        
        addSubview(oracle_name)
        addSubview(cardPicture)
        addSubview(card_name)
        
        addConsWithFormat(format: "H:|-8-[v0]-8-|", views: cardPicture)
        addConsWithFormat(format: "H:|-8-[v0]-8-|", views: oracle_name)
        addConsWithFormat(format: "H:|-8-[v0]-8-|", views: card_name)
        addConsWithFormat(format: "V:|-8-[v0]-8-[v1(200)]-8-[v2]-8-|", views: card_name,cardPicture, oracle_name)
        
    }
}
