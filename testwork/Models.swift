//
//  Models.swift
//  testwork
//
//  Created by muxammed on 11/11/2019.
//  Copyright © 2019 muxammed. All rights reserved.
//

import UIKit

enum Result <T>{
    case Success(T)
    case Error(String)
}

struct SearchResult:Codable {
    let object: String
    let totalCards: Int
    let hasMore: Bool
    let nextPage: String?
    let data: [Card]?
    
    enum CodingKeys: String, CodingKey {
        case object
        case totalCards = "total_cards"
        case hasMore = "has_more"
        case nextPage = "next_page"
        case data
    }
}

struct Card: Codable {
    
    let id: String
    let name: String
    let imageUris: ImageUris?
    let oracleText: String?
    
    
    enum CodingKeys: String, CodingKey {
        case id,name
        case imageUris = "image_uris"
        case oracleText = "oracle_text"
        
    }
}

struct ImageUris: Codable {
    let small, normal, large: String
    let png: String
    let artCrop, borderCrop: String
    
    enum CodingKeys: String, CodingKey {
        case small, normal, large, png
        case artCrop = "art_crop"
        case borderCrop = "border_crop"
    }
}
